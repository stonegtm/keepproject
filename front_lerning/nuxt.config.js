import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: 'Ufabet | %s',
    title: 'คาสิโนออนไลน์อันดับ 1 ที่ทุกคนไว้วางใจ',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'คาสิโนออนไลน์อันดับ 1 ที่ทุกคนไว้วางใจ', name: 'คาสิโนออนไลน์อันดับ 1 ที่ทุกคนไว้วางใจ', content: 'คาสิโนออนไลน์อันดับ 1 ที่ทุกคนไว้วางใจ' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/monkey.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/global-main.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/fontawesome',
    '@nuxtjs/vuetify',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/bootstrap-vue',
    '@nuxtjs/axios'

  ],

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },
  axios: {
    proxy: true, // Can be also an object with default options

  },

  proxy: {
    // Simple proxy
    '/api': 'http://178.128.21.58:5700',
    '/todos': 'https://jsonplaceholder.typicode.com',
    '/mountains': 'https://api.nuxtjs.dev',
    '/planetary': 'https://api.nasa.gov',
    // '/ghost': 'http://ghost:2368',
    '/ghost': 'http://localhost:3001',

    // http://localhost:3001/ghost/api/v3/content/posts/?key=6f23bfc104035252692ca7121e
  },


}

